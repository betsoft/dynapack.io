import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../global.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'dart:io' show Platform;

class MenuActions extends StatelessWidget {
  const MenuActions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
        onSelected: (item) => onSelected(context, item),
        itemBuilder: (context) => [
              PopupMenuItem(
                value: 0,
                child: Text('LogOut'),
              )
            ]);
  }

  onSelected(BuildContext context, int index) {
    switch (index) {
      case 0:
        showDialog(
            context: context,
            builder: (context) => Platform.isIOS
                ? CupertinoAlertDialog(
                    title: Text(AppLocalizations.of(context).logout),
                    actions: [
                      CupertinoDialogAction(
                        child: Text(AppLocalizations.of(context).no),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                      ),
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        child: Text(AppLocalizations.of(context).yes),
                        onPressed: () {
                          logout(context);
                        },
                      )

                    ],
                  )
                : AlertDialog(
                    title: Text(AppLocalizations.of(context).logout),
                    actions: [
                      TextButton(child: Text(AppLocalizations.of(context).no), onPressed: () => Navigator.of(context, rootNavigator: true).pop(),),
                      TextButton(child: Text(AppLocalizations.of(context).yes), onPressed: () => logout(context))
                      ],
                  ));
        break;
    }
  }
}
