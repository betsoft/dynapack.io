import 'package:flutter/cupertino.dart';

class AnagraphicTitle extends StatelessWidget {

  final String title;
  const AnagraphicTitle({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 8, 0, 0),
      child: Text(title, style: TextStyle(fontWeight: FontWeight.bold,)),
    );
  }
}
