import 'package:dynapack/global.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DeliverView extends StatefulWidget {
  const DeliverView({Key? key}) : super(key: key);

  @override
  _DeliverViewState createState() => _DeliverViewState();
}

class _DeliverViewState extends State<DeliverView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Crea Documento"),
        ),
        body: Column(
          children: [
            Expanded(
                child: ListView.builder(
              padding: EdgeInsets.only(top: 0),
              shrinkWrap: true,
              itemCount: deliverRows.length,
              itemBuilder: (context, index) {
                return Stack(
                  clipBehavior: Clip.none,
                  alignment: Alignment.topRight,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(10)),
                              ),
                              child: Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            deliverRows
                                                .elementAt(index)
                                                .productTransaction!
                                                .documentNumber,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          deliverRows
                                              .elementAt(index)
                                              .productTransaction!
                                              .documentDate,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                bottom: BorderSide(
                                    width: 2, color: Colors.blueGrey),
                              )),
                              child: Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          deliverRows
                                              .elementAt(index)
                                              .delivery!
                                              .salesOrderRow!
                                              .order!
                                              .customer
                                              .Code,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          deliverRows
                                              .elementAt(index)
                                              .delivery!
                                              .salesOrderRow!
                                              .order!
                                              .customer
                                              .Name1,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ),
                                      deliverRows
                                                  .elementAt(index)
                                                  .delivery!
                                                  .salesOrderRow!
                                                  .order!
                                                  .documentNumber ==
                                              'null'
                                          ? Container()
                                          : Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                deliverRows
                                                    .elementAt(index)
                                                    .delivery!
                                                    .salesOrderRow!
                                                    .order!
                                                    .documentNumber,
                                                style: TextStyle(
                                                    color: Colors.black),
                                              ),
                                            ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                      decoration: BoxDecoration(
                                          border: Border(
                                              right: BorderSide(
                                                  width: 2,
                                                  color: Colors.blueGrey))),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text("Quantita Principale"),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(quantities
                                                .elementAt(index)
                                                .toString()),
                                          )
                                        ],
                                      )),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text("Numero UDC"),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                            udcs.elementAt(index).toString()),
                                      )
                                    ],
                                  )),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                        top: 0.0,
                        right: 0.0,
                        child: GestureDetector(onTap: (){
                          deliverRows.removeAt(index);
                          quantities.removeAt(index);
                          udcs.removeAt(index);
                          setState(() {

                          });
                        }, child: Image(image: AssetImage('assets/images/remove.png'), width: 20, height: 20,)) /*Icon(Icons.remove_circle_rounded,
                            color: Colors.red, ),*/
                        ),
                  ],
                );
              },
            )),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.95,
                height: MediaQuery.of(context).size.width * 0.15,
                decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.blue),
                      top: BorderSide(color: Colors.blue),
                      right: BorderSide(color: Colors.blue),
                      left: BorderSide(color: Colors.blue),
                    ),
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Icon(Icons.savings, color: Colors.blue),
              ),

            )
          ],
        ));
  }
}
