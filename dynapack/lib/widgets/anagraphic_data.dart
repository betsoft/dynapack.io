import 'package:flutter/cupertino.dart';

class AnagraphicData extends StatelessWidget {

  final String data;
  const AnagraphicData({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 8, 0, 0),
      child: Text(data != 'null' ? data : ''),
    );
  }
}
