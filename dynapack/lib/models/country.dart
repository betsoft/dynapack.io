
class Country {
  final int DynaId;
  final String id;
  final String code;
  final String name;
  final String isoCode;
  final String phonePrefix;
  final int deliveryDelay;

  Country(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.name,
        required this.isoCode,
        required this.phonePrefix,
        required this.deliveryDelay});

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        name: json['Name'].toString(),
        isoCode: json['IsoCode'].toString(),
        phonePrefix: json['PhonePrefix'].toString(),
        deliveryDelay: int.tryParse(json['DeliveryDelay'].toString()) ?? 0);
  }
}
