class Certification {
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;

  Certification({
    required this.DynaId,
    required this.id,
    required this.code,
    required this.externalCode
  });

  factory Certification.fromJson(Map<String, dynamic> json){
    return Certification(DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        externalCode: json['ExternalCode'].toString());
  }
}