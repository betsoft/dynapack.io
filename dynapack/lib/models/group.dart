class Group{

  int dynaId;
  String id;
  String code;
  String externalCode;
  String description;

  Group({
    required this.dynaId,
    required this.id,
    required this.code,
    required this.externalCode,
    required this.description
  });

  factory Group.fromJson(Map<String, dynamic> json){
    return Group(dynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        externalCode: json['ExternalCode'].toString(),
        description: json['Description'].toString());

  }

}