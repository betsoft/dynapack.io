
import 'country.dart';

class Region {
  final int DynaId;
  final String id;
  final String code;
  final String name;
  final int deliveryDelay;
  final Country? country;

  Region(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.name,
        required this.deliveryDelay,
        required this.country});

  factory Region.fromJson(Map<String, dynamic> json) {
    return Region(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        name: json['Name'].toString(),
        deliveryDelay: int.tryParse(json['DeliveryDelay'].toString()) ?? 0,
        country: json['Country'] == null ? null : Country.fromJson(json['Country']));
  }
}