class Vat {
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;
  final String description;
  final double percentage;

  Vat(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.externalCode,
        required this.description,
        required this.percentage});

  factory Vat.fromJson(Map<String, dynamic> json) {
    return Vat(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        externalCode: json['ExternalCode'].toString(),
        description: json['Description'].toString(),
        percentage: double.tryParse(json['Percentage'].toString()) ?? 0);
  }
}