
class Carriage {
  final int DynaId;
  final String id;
  final String code;
  final String description;

  Carriage(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.description});

  factory Carriage.fromJson(Map<String, dynamic> json) {
    return Carriage(
      DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
      id: json['id'].toString(),
      code: json['Code'].toString(),
      description: json['Description'].toString(),
    );
  }
}
