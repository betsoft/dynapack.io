import 'quality.dart';
import 'vat.dart';

import 'category.dart';
import 'conai.dart';
import 'group.dart';

class Products {
  int createdAt;
  int updatedAt;
  String id;
  int dynaId;
  String code;
  String externalCode;
  String description1;
  String description2;
  String mainMeasureUnit;
  String alternativeMeasureUnit;
  String conaiWeight;
  int dimension1;
  int dimension2;
  int dimension3;
  int grammage;
  String status;
  Vat vat;
  Quality? quality;
  Conai? conai;
  Group? group;
  Category? category;

  Products({
    required this.createdAt,
    required this.updatedAt,
    required this.id,
    required this.dynaId,
    required this.code,
    required this.externalCode,
    required this.description1,
    required this.description2,
    required this.mainMeasureUnit,
    required this.alternativeMeasureUnit,
    required this.conaiWeight,
    required this.dimension1,
    required this.dimension2,
    required this.dimension3,
    required this.grammage,
    required this.status,
    required this.vat,
    required this.quality,
    required this.conai,
    required this.group,
    required this.category,
  });

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
      createdAt: json['createdAt'] ?? 0,
      updatedAt: json['updatedAt'] ?? 0,
      id: json['id'].toString(),
      dynaId: json['DynaId'] ?? 0,
      code: json['Code'].toString(),
      externalCode: json['ExternalCode'].toString(),
      description1: json['Description1'].toString(),
      description2: json['Description2'].toString(),
      mainMeasureUnit: json['MainMeasureUnit'].toString(),
      alternativeMeasureUnit: json['AlternativeMeasureUnit'].toString(),
      conaiWeight: json['ConaiWeight'].toString(),
      dimension1: json['Dimension1'] ?? 0,
      dimension2: json['Dimension2'] ?? 0,
      dimension3: json['Dimension3'] ?? 0,
      grammage: json['Grammage'] ?? 0,
      status: json['Status'].toString(),
      vat: Vat.fromJson(json['Vat']),
      quality: json['Quality'] == null ? null : Quality.fromJson(json['Quality']),
      conai: json['Conai'] == null ? null : Conai.fromJson(json['Conai']),
      group: json['ProductGroup'] == null ? null : Group.fromJson(json['ProductGroup']),
      category: json['Category'] == null ? null : Category.fromJson(json['ProductCategory'])


    );


  }
}
