import 'product_transaction.dart';

import 'certification.dart';
import 'production_order_row.dart';
import 'products.dart';
import 'vat.dart';

import 'category.dart';
import 'conai.dart';
import 'delivery.dart';

class ProductTransactionRows{
  final int DynaId;
  final String id;
  final int rowNumber;
  final ProductionOrderRow? productionOrderRow;
  final bool isClosed;
  final Products? product;
  final String description1;
  final String description2;
  final String description3;
  final double dimension1;
  final double dimension2;
  final double dimension3;
  final double mainQuantity;
  final String mainMeasureUnit;
  final double alternativequantity;
  final String alternativeMeasureUnit;
  final double invoiceQuantity;
  final String invoiceMeasureUnit;
  final int udcNumber;
  final double discount1;
  final double discount2;
  final int grammage;
  final Category? productCategory;
  final Vat? vat;
  final Delivery? delivery;
  final Conai? conai;
  final double conaiWeight;
  final Certification? certification;
  final bool isPalletMovement;
  final double costPerUnit;
  final double expensePerUnit;
  final double preExpensePerUnit;
  final String conaiPriceDate;
  final String expirationDate;
  final ProductTransaction? productTransaction;
  
  ProductTransactionRows({
    required this.DynaId,
    required this.id,
    required this.rowNumber,
    required this.productionOrderRow,
    required this.isClosed,
    required this.product,
    required this.description1,
    required this.description2,
    required this.description3,
    required this.dimension1,
    required this.dimension2,
    required this.dimension3,
    required this.mainQuantity,
    required this.mainMeasureUnit,
    required this.alternativequantity,
    required this.alternativeMeasureUnit,
    required this.invoiceQuantity,
    required this.invoiceMeasureUnit,
    required this.udcNumber,
    required this.discount1,
    required this.discount2,
    required this.grammage,
    required this.productCategory,
    required this.vat,
    required this.delivery,
    required this.conai,
    required this.conaiWeight,
    required this.certification,
    required this.isPalletMovement,
    required this.costPerUnit,
    required this.expensePerUnit,
    required this.preExpensePerUnit,
    required this.conaiPriceDate,
    required this.expirationDate,
    required this.productTransaction,
  });

  factory ProductTransactionRows.fromJson(Map<String, dynamic> json) {
    return ProductTransactionRows(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        rowNumber: int.tryParse(json['DynaId'].toString()) ?? 0,
        productionOrderRow: (json['ProductionOrderRow'] == null ) ? null : ProductionOrderRow.fromJson(json['ProductionOrderRow']),
        isClosed: json['IsClosed'],
        product: Products.fromJson(json['Product']),
        description1: json['Description1'].toString(),
        description2: json['Description2'].toString(),
        description3: json['Description3'].toString(),
        dimension1: json['Dimension1'].toDouble() ?? 0,
        dimension2: json['Dimension2'].toDouble() ?? 0,
        dimension3: json['Dimension3'].toDouble() ?? 0,
        mainQuantity: json['MainQuantity'].toDouble() ?? 0,
        mainMeasureUnit: json['MainMeasureUnit'].toString(),
        alternativequantity: json['AlternativeQuantity'].toDouble() ?? 0,
        alternativeMeasureUnit: json['AlternativeMeasureUnit'].toString(),
        invoiceQuantity: json['InvoiceQuantity'].toDouble() ?? 0,
        invoiceMeasureUnit: json['InvoiceMeasureUnit'].toString(),
        udcNumber: json['UdcNumber'] ?? 0,
        discount1: json['Discount1'].toDouble() ?? 0,
        discount2: json['Discount2'].toDouble() ?? 0,
        grammage: json['Grammage'] ?? 0,
        productCategory: Category.fromJson(json['ProductCategory']),
        vat: json['Vat'] == null ? null : Vat.fromJson(json['Vat']),
        delivery: json['Delivery'] == null ? null :Delivery.fromJson(json['Delivery']),
        conai: json['Conai'] == null ? null : Conai.fromJson(json['Conai']),
        conaiWeight: json['ConaiWeight'] == null ? 0.0 : json['ConaiWeight'].toDouble(),
        certification: json['Certification'] == null ? null : Certification.fromJson(json['Certification']),
        isPalletMovement: json['IsPalletMovement'],
        costPerUnit: json['CostPerUnit'] == null ? 0.0 : json['CostPerUnit'].toDouble() ,
        expensePerUnit: json['ExpensePerUnit'] == null ? 0.0 : json['ExpensePerUnit'].toDouble(),
        preExpensePerUnit: json['PreExpensePerUnit'] == null ? 0.0 : json['PreExpensePerUnit'].toDouble(),
        conaiPriceDate: json['ConaiPriceDate'].toString(),
        expirationDate: json['ExpirationDate'].toString(),
        productTransaction: ProductTransaction.fromJson(json['ProductTransaction']));

  }
}
