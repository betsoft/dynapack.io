import 'products.dart';

import 'certification.dart';

import 'order.dart';

class ProductionOrderRow {
  final int DynaId;
  final String id;
  final Products? product;
  final int jobNumber;
  final int rowNumber;
  final double quantity;
  final String customerReference;
  final String customerReferenceDate;
  final bool isClosed;
  final Certification? certification;
  final Order? order;

  ProductionOrderRow({
    required this.DynaId,
    required this.id,
    required this.product,
    required this.jobNumber,
    required this.rowNumber,
    required this.quantity,
    required this.customerReference,
    required this.customerReferenceDate,
    required this.isClosed,
    required this.certification,
    required this.order,
  });

  factory ProductionOrderRow.fromJson(Map<String, dynamic> json) {
    return ProductionOrderRow(
        DynaId: json['DynaId'] ?? 0,
        id: json['id'],
        product: json['Product'] == null ? null : Products.fromJson(json['Product']),
        jobNumber: json['JobNumber'] ?? 0,
        rowNumber: json['RowNumber'] ?? 0,
        quantity: json['quantity'] ?? 0,
        customerReference: json['CustomerReference'].toString(),
        customerReferenceDate: json['CustomerReferenceDate'].toString(),
        isClosed: json['IsClosed'],
        certification: json['Certification'] == null ? null :Certification.fromJson(json['Certification']),
        order: json['Order'] == null ? null : Order.fromJson(json['Order']));
  }
}
