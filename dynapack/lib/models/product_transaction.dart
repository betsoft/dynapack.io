import 'carriage.dart';
import 'currency.dart';
import 'deadline.dart';
import 'destination.dart';
import 'payment.dart';
import 'supplier.dart';
import 'transport.dart';

import 'customers.dart';
import 'deposit.dart';
import 'machine.dart';
import 'transaction_type.dart';

class ProductTransaction {
  final int DynaId;
  final String id;
  final TransactionType? transactionType;
  final Supplier? supplier;
  final Customers? customer;
  final Destination? destination;
  final Deposit? deposit;
  final Deposit? holderDeposit;
  final Machine? machine;
  final String documentNumber;
  final String documentDate;
  final Payment? paymentMethod;
  final Deadline? deadline;
  final Carriage? carriage;
  final Transport? transport;
  final double discount1;
  final double discount2;
  final String invoiceNumeber;
  final String invoiceDate;
  final String issueDate;
  final Currency? currency;
  final double exchangeRateValue;
  final String state;

  ProductTransaction({
    required this.DynaId,
    required this.id,
    required this.transactionType,
    required this.supplier,
    required this.customer,
    required this.destination,
    required this.deposit,
    required this.holderDeposit,
    required this.machine,
    required this.documentNumber,
    required this.documentDate,
    required this.paymentMethod,
    required this.deadline,
    required this.carriage,
    required this.transport,
    required this.discount1,
    required this.discount2,
    required this.invoiceNumeber,
    required this.invoiceDate,
    required this.issueDate,
    required this.currency,
    required this.exchangeRateValue,
    required this.state,
  });

  factory ProductTransaction.fromJson(Map<String, dynamic> json) {
    return ProductTransaction(
        DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        transactionType: json['TransactionType'] == null ? null : TransactionType.fromJson(json['TransactionType']),
        supplier: json['Supplier'] == null ? null : Supplier.fromJson(json['Supplier']),
        customer: json['Customers'] == null ? null : Customers.fromJson(json['Customer']),
        destination: json['Destination'] == null ? null : Destination.fromJson(json['Destination']),
        deposit: json['Deposit'] == null ? null : Deposit.fromJson(json['Deposit']),
        holderDeposit: json['HolderDeposit'] == null ? null : Deposit.fromJson(json['HolderDeposit']),
        machine: json['Machine'] == null ? null : Machine.fromJson(json['Machine']),
        documentNumber: json['DocumentNumber'].toString(),
        documentDate: json['DocumentDate'].toString(),
        paymentMethod: json['PaymentMethod'] == null ? null : Payment.fromJson(json['PaymentMethod']),
        deadline: json['Deadline'] == null ? null : Deadline.fromJson(json['Deadline']),
        carriage: json['Carriage'] == null ? null : Carriage.fromJson(json['CarriageType']),
        transport: json['Transport'] == null ? null : Transport.fromJson(json['TransportType']),
        discount1: json['Discount1'] == null ? 0.0 : json['Discount1'].toDouble(),
        discount2: json['Discount2'] == null ? 0.0 : json['Discount2'].toDouble(),
        invoiceNumeber: json['InvoiceNumber'].toString(),
        invoiceDate: json['InvoiceDate'].toString(),
        issueDate: json['IssueDate'].toString(),
        currency: json['Currency'] == null ? null : Currency.fromJson(json['Currency']),
        exchangeRateValue: json['ExchangeRateValue'] == null ? 0.0 : json['ExchangeRateValue'].toDouble(),
        state: json['State'].toString());
  }
}
