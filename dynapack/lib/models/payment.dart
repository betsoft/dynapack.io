
class Payment {
  final int DynaId;
  final String id;
  final String code;
  final String description;
  final String externalCode;
  final double discount1;
  final double discount2;

  Payment(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.description,
        required this.externalCode,
        required this.discount1,
        required this.discount2});

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        description: json['Description'].toString(),
        externalCode: json['ExternalCode'].toString(),
        discount1: double.tryParse(json['Discount1'].toString()) ?? 0,
        discount2: double.tryParse(json['Discount2'].toString()) ?? 0);
  }
}


