class Quality{

  int dynaId;
  String id;
  String code;
  String description;

  Quality({
    required this.dynaId,
    required this.id,
    required this.code,
    required this.description
  });

  factory Quality.fromJson(Map<String, dynamic> json){
    return Quality(dynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        description: json['Description'].toString());

  }

}