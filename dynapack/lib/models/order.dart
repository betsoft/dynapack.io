import 'customers.dart';

class Order {
  final int DynaId;
  final String id;
  final Customers customer;
  final String documentNumber;
  final String date;
  final String customerReference;
  final String customerReferenceRow;
  final String customerReferenceDate;

  Order({
    required this.DynaId,
    required this.id,
    required this.customer,
    required this.documentNumber,
    required this.date,
    required this.customerReference,
    required this.customerReferenceRow,
    required this.customerReferenceDate,

  });

  factory Order.fromJson(Map<String, dynamic> json){
    return Order(DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        customer: Customers.fromJson(json['Customer']),
        documentNumber: json['CustomerNumber'].toString(),
        date: json['date'].toString(),
        customerReference: json['CustomerReference'].toString(),
        customerReferenceRow: json['CustomerReferenceRow'].toString(),
        customerReferenceDate: json['CustomerReferenceDate'].toString());
  }
}