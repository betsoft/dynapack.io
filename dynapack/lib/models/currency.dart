class Currency {
  final int DynaId;
  final String id;
  final String code;
  final String description;

  Currency(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.description});

  factory Currency.fromJson(Map<String, dynamic> json) {
    return Currency(
      DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
      id: json['id'].toString(),
      code: json['Code'].toString(),
      description: json['Description'].toString(),
    );
  }
}