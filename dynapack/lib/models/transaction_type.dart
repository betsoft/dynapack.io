class TransactionType {
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;
  final String description;

  TransactionType({
    required this.DynaId,
    required this.id,
    required this.code,
    required this.externalCode,
    required this.description
  });

  factory TransactionType.fromJson(Map<String, dynamic> json){
    return TransactionType(DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        externalCode: json['ExternalCode'].toString(),
        description: json['Description'].toString());
  }
}