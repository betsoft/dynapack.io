import 'city.dart';

class Supplier {
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;
  final String name1;
  final String name2;
  final String name3;
  final String cap;
  final String address;
  final String vatNumber;
  final String fiscalCode;
  final String phoneNumber;
  final String faxNumber;
  final String email;
  final double discount1;
  final double discount2;
  final double distance;
  final City city;

  Supplier({
    required this.DynaId,
    required this.id,
    required this.code,
    required this.externalCode,
    required this.name1,
    required this.name2,
    required this.name3,
    required this.cap,
    required this.address,
    required this.vatNumber,
    required this.fiscalCode,
    required this.phoneNumber,
    required this.faxNumber,
    required this.email,
    required this.discount1,
    required this.discount2,
    required this.distance,
    required this.city,
  });

  factory Supplier.fromJson(Map<String, dynamic> json) {
    return Supplier(
        DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        externalCode: json['ExternalCode'].toString(),
        name1: json['Name1'].toString(),
        name2: json['Name2'].toString(),
        name3: json['Name3'].toString(),
        cap: json['Cap'].toString(),
        address: json['Address'].toString(),
        vatNumber: json['VatNumber'].toString(),
        fiscalCode: json['FiscalCode'].toString(),
        phoneNumber: json['PhoneNumber'].toString(),
        faxNumber: json['FaxNumber'].toString(),
        email: json['Email'].toString(),
        discount1: json['Discount1'] ?? 0,
        discount2: json['Discount2'] ?? 0,
        distance: json['Distance'] ?? 0,
        city: City.fromJson(json['City']));
  }
}
