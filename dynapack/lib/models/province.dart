import 'region.dart';

class Province {
  final int DynaId;
  final String id;
  final String code;
  final String name;
  final int deliveryDelay;
  final Region? region;

  Province(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.name,
        required this.deliveryDelay,
        required this.region});

  factory Province.fromJson(Map<String, dynamic> json) {
    return Province(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        name: json['Name'].toString(),
        deliveryDelay: int.tryParse(json['DeliveryDelay'].toString()) ?? 0,
        region: json['Region'] == null ? null : Region.fromJson(json['Region']));
  }
}