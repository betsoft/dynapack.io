import 'city.dart';

class Destination{
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;
  final String name1;
  final String name2;
  final String name3;
  final String cap;
  final String address;
  final City city;
  
  Destination({
    required this.DynaId,
    required this.id,
    required this.code,
    required this.externalCode,
    required this.name1,
    required this.name2,
    required this.name3,
    required this.cap,
    required this.address,
    required this.city,
});

  factory Destination.fromJson(Map<String, dynamic> json) {
    return Destination(
      DynaId: json['DynaId'] ?? 0,
      id: json['id'].toString(),
      code: json['ShippingQuanitity'].toString(),
      externalCode: json['ShippingDate'].toString(),
      name1: json['Name1'].toString(),
      name2: json['Name2'].toString(),
      name3: json['Name3'].toString(),
      cap: json['Cap'].toString(),
      address: json['Address'].toString(),
      city: City.fromJson(json['City']),

    );
  }
  
}