import 'production_order_row.dart';
import 'sales_order_row.dart';

import 'destination.dart';

class Delivery{
  final int DynaId;
  final String id;
  final int shippingQuantity;
  final String shippingDate;
  final SalesOrderRow? salesOrderRow;
  final ProductionOrderRow? productionOrderRow;
  final Destination? destination;
  
  Delivery({
    required this.DynaId,
    required this.id,
    required this.shippingQuantity,
    required this.shippingDate,
    required this.salesOrderRow,
    required this.productionOrderRow,
    required this.destination,
  });

  factory Delivery.fromJson(Map<String, dynamic> json) {
    return Delivery(
        DynaId: json['DynaId'] ?? 0,
        id: json['id'].toString(),
        shippingQuantity: json['ShippingQuanitity'] ?? 0,
        shippingDate: json['ShippingDate'].toString(),
        salesOrderRow: json['SalesOrderRow'] == null ? null : SalesOrderRow.fromJson(json['SalesOrderRow']),
        productionOrderRow: json['ProductionOrderRow'] == null ? null : ProductionOrderRow.fromJson(json['ProductionOrderRow']),
        destination: json['Destination'] == null ? null : Destination.fromJson(json['Destination']),
    );
  }
}