
import 'payment.dart';
import 'transport.dart';

import 'carriage.dart';
import 'city.dart';
import 'currency.dart';
import 'deadline.dart';
import 'vat.dart';
class Customers {
  //final int createdAt;
  //final int updateAt;
  final String id;
  final int DynaId;
  final String Code;
  final String ExternalCode;
  final String Name1;
  final String Name2;
  final String Name3;
  final String Cap;
  final String Address;
  final String VatNumber;
  final String FiscalCode;
  final String PhoneNumber;
  final String FaxNumber;
  final String Email;
  final String Discount1;
  final String Discount2;
  final String Distance;
  final String CustomerType;
  final Payment? payment;
  final City? city;
  final Carriage? carriage;
  final Transport? transport;
  final Currency? currency;
  final Deadline? deadline;
  final Vat? vat;

  Customers(
      {
        //required this.createdAt,
        //required this.updateAt,
        required this.id,
        required this.DynaId,
        required this.Code,
        required this.ExternalCode,
        required this.Name1,
        required this.Name2,
        required this.Name3,
        required this.Cap,
        required this.Address,
        required this.VatNumber,
        required this.FiscalCode,
        required this.PhoneNumber,
        required this.FaxNumber,
        required this.Email,
        required this.Discount1,
        required this.Discount2,
        required this.Distance,
        required this.CustomerType,
        required this.payment,
        required this.city,
        required this.carriage,
        required this.transport,
        required this.currency,
        required this.deadline,
        required this.vat});

  factory Customers.fromJson(Map<String, dynamic> json) {
    return Customers(
      //createdAt: int.tryParse(json['createdAt']) ?? 0,
      //updateAt: int.tryParse(json['updateAt']) ?? 0,
      id: json['id'].toString(),
      DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
      Code: json['Code'].toString(),
      ExternalCode: json['ExternalCode'].toString(),
      Name1: json['Name1'].toString(),
      Name2: json['Name2'].toString(),
      Name3: json['Name3'].toString(),
      Cap: json['Cap'].toString(),
      Address: json['Address'].toString(),
      VatNumber: json['VatNumber'].toString(),
      FiscalCode: json['FiscalCode'].toString(),
      PhoneNumber: json['PhoneNumber'].toString(),
      FaxNumber: json['FaxNumber'].toString(),
      Email: json['Email'].toString(),
      Discount1: json['Discount1'].toString(),
      Discount2: json['Discount2'].toString(),
      Distance: json['Distance'].toString(),
      CustomerType: json['CustomerType'].toString(),
      payment: json['PaymentMethod'] == null ? null : Payment.fromJson(json['PaymentMethod']),
      city: json['City'] == null ? null : City.fromJson(json['City']),
      carriage: json['CarriageType'] == null ? null : Carriage.fromJson(json['CarriageType']),
      transport: json['TransportType'] == null ? null : Transport.fromJson(json['TransportType']),
      currency: json['Currency'] == null ? null : Currency.fromJson(json['Currency']),
      deadline: json['Deadline'] == null ? null : Deadline.fromJson(json['Deadline']),
      vat: json['Vat'] == null ? null : Vat.fromJson(json['Vat']),
    );
  }
}
