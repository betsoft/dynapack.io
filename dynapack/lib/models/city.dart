
import 'province.dart';

class City {
  final int DynaId;
  final String id;
  final String code;
  final String name;
  final int deliveryDelay;
  final Province? province;

  City(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.name,
        required this.deliveryDelay,
        required this.province});

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        name: json['Name'].toString(),
        deliveryDelay: int.tryParse(json['DeliveryDelay'].toString()) ?? 0,
        province: json['Province'] == null ? null : Province.fromJson(json['Province']));
  }
}



