class Machine {
  final int DynaId;
  final String id;
  final String code;
  final String description;

  Machine(
      {required this.DynaId,
      required this.id,
      required this.code,
      required this.description});

  factory Machine.fromJson(Map<String, dynamic> json) {
    return Machine(
        DynaId: json['DynaId'] ?? 0,
        id: json['Code'].toString(),
        code: json['Code'].toString(),
        description: json['Description'].toString());
  }
}
