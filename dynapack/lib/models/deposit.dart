class Deposit{
  final int DynaId;
  final String id;
  final String code;
  final String description;

  Deposit({
    required this.DynaId,
    required this.id,
    required this.code,
    required this.description
});

  factory Deposit.fromJson(Map<String, dynamic> json){
    return Deposit(
      DynaId: json['DynaId'] ?? 0,
      id: json['id'].toString(),
      code: json['Code'].toString(),
      description: json['Description'].toString()
    );
  }
}