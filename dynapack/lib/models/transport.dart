
class Transport {
  final int DynaId;
  final String id;
  final String code;
  final String description;
  final int copiesNumber;

  Transport(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.description,
        required this.copiesNumber});

  factory Transport.fromJson(Map<String, dynamic> json) {
    return Transport(
        DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
        id: json['id'].toString(),
        code: json['Code'].toString(),
        description: json['Description'].toString(),
        copiesNumber: int.tryParse(json['CopiesNumber'].toString()) ?? 0);
  }
}
