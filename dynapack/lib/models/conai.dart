class Conai{
  int dynaId;
  String id;
  String code;
  String description;

  Conai({
    required this.dynaId,
    required this.id,
    required this.code,
    required this.description
});

  factory Conai.fromJson(Map<String, dynamic> json){
    return Conai(dynaId: json['DynaId'] ?? 0,
      id: json['id'].toString(),
      code: json['Code'].toString(),
      description: json['Description'].toString());

  }
}