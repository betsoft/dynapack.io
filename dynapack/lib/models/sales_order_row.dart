import 'certification.dart';
import 'order.dart';
import 'products.dart';

class SalesOrderRow {
  final int DynaId;
  final String id;
  final Products? product;
  final int rowNumber;
  final double quantity;
  final String customerReference;
  final String customerReferenceRow;
  final String customerReferenceDate;
  final bool isClosed;
  final Certification? certification;
  final Order? order;

  SalesOrderRow({
    required this.DynaId,
    required this.id,
    required this.product,
    required this.rowNumber,
    required this.quantity,
    required this.customerReference,
    required this.customerReferenceRow,
    required this.customerReferenceDate,
    required this.isClosed,
    required this.certification,
    required this.order,
  });

  factory SalesOrderRow.fromJson(Map<String, dynamic> json) {
    return SalesOrderRow(
      DynaId: json['DynaId'] ?? 0,
      id: json['id'].toString(),
      product: json['Product'] == null ? null : Products.fromJson(json['Product']),
      rowNumber: json['RowNumber'] ?? 0,
      quantity: json['Quantity'].toDouble() ?? 0,
      customerReference: json['CustomerReference'].toString(),
      customerReferenceDate: json['CustomerReferenceDate'].toString(),
      customerReferenceRow: json['CustomerReferenceRow'].toString(),
      isClosed: json['IsClosed'],
      certification: json['Certification'] == null ? null : Certification.fromJson(json['Certification']),
      order: json['Order'] == null ? null : Order.fromJson(json['Order']),
    );
  }

}