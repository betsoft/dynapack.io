class Deadline {
  final int DynaId;
  final String id;
  final String code;
  final String externalCode;
  final String description;

  Deadline(
      {required this.DynaId,
        required this.id,
        required this.code,
        required this.externalCode,
        required this.description});

  factory Deadline.fromJson(Map<String, dynamic> json) {
    return Deadline(
      DynaId: int.tryParse(json['DynaId'].toString()) ?? 0,
      id: json['id'].toString(),
      code: json['Code'].toString(),
      externalCode: json['ExternalCode'].toString(),
      description: json['Description'].toString(),
    );
  }
}