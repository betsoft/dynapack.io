import 'package:dynapack/customers_view.dart';
import 'package:dynapack/login.dart';
import 'package:dynapack/product_transaction_rows_view.dart';
import 'package:dynapack/products_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'global.dart';

class AppDrawer extends StatelessWidget {
  AppDrawer(this.page);

  late String page;

  /*void logout(BuildContext context) async {
    await storage.delete(key: "username");
    await storage.delete(key: "password");
    await prefs.remove("isLogged");
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Login()));
  }*/

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          ListTile(
            tileColor: page == 'Customers'
                ? Colors.blue.withOpacity(0.6)
                : Colors.transparent,
            title: Text(AppLocalizations.of(context).customers),
            onTap: () {
              if (page == 'Customers') {
                Navigator.pop(context);
              } else {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CustomersView(
                              title: 'Dynapack',
                            )));
              }
            },
          ),
          ListTile(
            tileColor: page == 'Products'
                ? Colors.blue.withOpacity(0.6)
                : Colors.transparent,
            title: Text(AppLocalizations.of(context).products),
            onTap: () {
              if (page == 'Products') {
                Navigator.pop(context);
              } else {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductsView()));
              }
            },
          ),ListTile(
            tileColor: page == 'ProductTransactionRows'
                ? Colors.blue.withOpacity(0.6)
                : Colors.transparent,
            title: Text(AppLocalizations.of(context).productTransactionRows),
            onTap: () {
              if (page == 'ProductTransactionRows') {
                Navigator.pop(context);
              } else {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductTransactionRowsView()));
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _createHeader() {
    return Container(
      decoration: BoxDecoration(color: Colors.blue),
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(color: Colors.blue),
          height: 56,
          child: DrawerHeader(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            decoration: BoxDecoration(
                color: Colors.blue,
                image: DecorationImage(
                    fit: BoxFit.contain,
                    image: AssetImage('assets/images/dynapack-logo-2021.png'))),
            child: null,
          ),
        ),
      ),
    );
  }
}
