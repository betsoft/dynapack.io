import 'dart:convert';

import 'package:dynapack/widgets/menu_actions.dart';
import 'package:dynapack/product_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

import 'drawer.dart';
import 'global.dart';
import 'models/products.dart';

class ProductsView extends StatefulWidget {
  const ProductsView({Key? key}) : super(key: key);

  @override
  _ProductsViewState createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  late Future<List<Products>> futureProducts;
  TextEditingController editingController = TextEditingController();
  List<Products> products = [];
  late List<Products> filter;
  final ScrollController _scrollController = ScrollController();
  int skip = 0;
  bool doItJustOnce = false;

  @override
  void initState() {
    super.initState();

    futureProducts = fetchProducts();
  }

  Future<List<Products>> fetchProducts() async {
    final response = await http.get(
        Uri.parse(
            server + '/products?limit=1&skip=' +
                skip.toString()),
        headers: headers);
    if (response.statusCode == 200) {
      List responseJson = jsonDecode(response.body);


      var risposta = responseJson.map((e) => Products.fromJson(e));
      skip += 30;
      return risposta.toList();
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      drawerEdgeDragWidth: MediaQuery.of(context).size.width,
      drawer: AppDrawer('Products'),
      /*appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          actions: [MenuActions()],
          title: Text('Dynapack'),
        ),
      ),*/
      body: FloatingSearchBar(
        hint: 'Cerca...',
        hintStyle: TextStyle(color: Colors.white),
        backgroundColor: Colors.blue,
        borderRadius: BorderRadius.circular(10),
        iconColor: Colors.white,
        scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
        transitionDuration: const Duration(milliseconds: 300),
        transitionCurve: Curves.easeInOut,
        physics: const BouncingScrollPhysics(),
        axisAlignment: isPortrait ? 0.0 : -1.0,
        openAxisAlignment: 0.0,
        width: isPortrait ? 600 : 1080,
        debounceDelay: const Duration(milliseconds: 500),
        onQueryChanged: (query) {
          // Call your model, bloc, controller here.
        },
        // Specify a custom transition to be used for
        // animating between opened and closed stated.
        transition: CircularFloatingSearchBarTransition(),
        actions: [
          MenuActions()
        ],
        builder: (context, transition) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Material(
              color: Colors.white,
              elevation: 4.0,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: Colors.accents.map((color) {
                  return Container(height: 112, color: color);
                }).toList(),
              ),
            ),
          );
        },
        body: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).viewPadding.top,),
                Expanded(
                  child: FutureBuilder<List<Products>>(
                    future: futureProducts,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        /*if (products.length > 0) {
                          products.addAll(snapshot.data!);
                        } else {*/
                          products = snapshot.data!;
                        //}
                        return ListView.builder(
                          shrinkWrap: true,
                          itemCount: products.length,
                          itemBuilder: (context, index) {
                            String name = AppLocalizations.of(context).code +
                                ': ' +
                                products.elementAt(index).code;
                            if (products.elementAt(index).description1 != 'null' &&
                                products.elementAt(index).description1 != '')
                              name += ', ' + products.elementAt(index).description1;
                            String codes =
                                AppLocalizations.of(context).externalCode +
                                    ': ' +
                                    products.elementAt(index).externalCode;
                            if (products.elementAt(index).description2 != 'null' &&
                                products.elementAt(index).description2 != '') {
                              codes +=
                                  ', ' + products.elementAt(index).description2;
                            }
                            return Slidable(
                                child: Container(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Product(
                                                product:
                                                    products.elementAt(index))),
                                      );
                                    },
                                    child: ListTile(
                                      title: Text(
                                        name,
                                        style:
                                            TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Text(codes,
                                          style: TextStyle(fontSize: 11)),
                                    ),
                                  ),
                                ),
                                actions: <Widget>[
                                  new IconSlideAction(
                                      icon: Icons.delete, color: Colors.red),
                                  new IconSlideAction(
                                    icon: Icons.archive,
                                    color: Colors.blueGrey,
                                  ),
                                ],
                                secondaryActions: <Widget>[
                                  new IconSlideAction(
                                      icon: Icons.delete, color: Colors.red),
                                  new IconSlideAction(
                                    icon: Icons.archive,
                                    color: Colors.blueGrey,
                                  ),
                                ],
                                actionPane: SlidableDrawerActionPane());
                          },
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return Center(
                          child: Container(
                              height: 100,
                              width: 100,
                              child: CircularProgressIndicator()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
