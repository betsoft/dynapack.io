import 'dart:convert';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:dynapack/customers_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'global.dart';
import 'main.dart';

late final SharedPreferences prefs;
bool prefsOk = false;

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController passwordController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();
  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.white);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    instantiatePreferences();
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 7), child: Text("Login...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> instantiatePreferences() async {
    if (Platform.isAndroid || Platform.isIOS) {
      storage = FlutterSecureStorage();
      prefs = await SharedPreferences.getInstance();
      setState(() {
        prefsOk = true;
      });
      if (prefs.containsKey('isLogged') && prefs.getInt('isLogged') == 1) {
        var localAuth = LocalAuthentication();
        bool didAuthenticate = await localAuth.authenticate(
            stickyAuth: true,
            localizedReason: "Autenticati per accedere all'app");

        if (didAuthenticate == true) {
          var username = await storage.read(key: 'username');
          var password = await storage.read(key: 'password');
          showLoaderDialog(context);
          var response = await login(username, password);
          Navigator.pop(context);
          if (response == 200) {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => CustomersView(
                          title: 'Dynapack',
                        )));
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(AppLocalizations.of(context).loginError),
            ));
          }
        }
      }
    }
  }

  Future<int> login(String username, String password) async {
    var map = new Map<String, dynamic>();
    map['username'] = username;
    map['password'] = password;
    final response =
        await http.post(Uri.parse(server + '/login'), body: json.encode(map));
    String rawCookie = response.headers['set-cookie'].toString();
    if (rawCookie != 'null') {
      int index = rawCookie.indexOf(';');
      headers['cookie'] =
          (index == -1) ? rawCookie : rawCookie.substring(0, index);
    }
    var responseFromJson = jsonDecode(response.body);
    if (responseFromJson['message'] == 'Username not found') {
      return 404;
    } else if (responseFromJson['message'] == 'Invalid Password') {
      return 401;
    } else if (responseFromJson['Username'] == username) {
      prefs.setInt('isLogged', 1);
      await storage.write(key: 'username', value: username);
      await storage.write(key: 'password', value: password);
      return 200;
    } else {
      return 500;
    }
  }

  Future<bool> showExitPopup() async {
    return await showDialog(
          //show confirm dialogue
          //the return value will be from "Yes" or "No" options
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Exit App'),
            content: Text('Do you want to exit an App?'),
            actions: [
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(false),
                //return false when click on "NO"
                child: Text('No'),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(true),
                //return true when click on "Yes"
                child: Text('Yes'),
              ),
            ],
          ),
        ) ??
        false; //if showDialouge had returned null, then return false
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
        obscureText: false,
        style: style,
        controller: usernameController,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.blue[200],
          prefixIcon: Icon(Icons.account_circle, color: Colors.white),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: 'Username',
          hintStyle: TextStyle(
              fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.white),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: Colors.white, width: 1.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: Colors.blueAccent, width: 2.5)),
        ));

    final passwordField = TextField(
      obscureText: true,
      controller: passwordController,
      style: style,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blue[200],
        prefixIcon: Icon(Icons.lock, color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Password',
        hintStyle: TextStyle(
            fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.white),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
            borderSide: BorderSide(color: Colors.white, width: 1)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
            borderSide: BorderSide(color: Colors.blueAccent, width: 2.5)),
      ),
    );

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.blue,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          showLoaderDialog(context);
          var utente = await login(usernameController.text.toString(),
              passwordController.text.toString());
          Navigator.pop(context);
          if (utente == 200) {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => CustomersView(title: 'Dynapack')));
          } else if (utente == 404) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(AppLocalizations.of(context).userNotFound),
            ));
          } else if (utente == 401) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(AppLocalizations.of(context).passwordNotValid),
            ));
          } else if (utente == 500) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(AppLocalizations.of(context).loginError),
            ));
          }
        },
        child: Text(
          "Login",
          textAlign: TextAlign.center,
          style:
              style.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );

    return WillPopScope(
      onWillPop: showExitPopup,
      child: Scaffold(
          body: Center(
              child: prefsOk == false
                  ? CircularProgressIndicator()
                  : Container(
                      color: Colors.blue,
                      child: Padding(
                        padding: const EdgeInsets.all(36.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 155.0,
                              child: Image.asset(
                                  'assets/images/dynapack-logo-2021.png',
                                  fit: BoxFit.contain),
                            ),
                            AnimatedContainer(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        25, 35, 25, 10),
                                    child: emailField,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        25, 10, 25, 15),
                                    child: passwordField,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        25, 10, 25, 15),
                                    child: loginButton,
                                  ),
                                ],
                              ),
                              duration: Duration(seconds: 1),
                              curve: Curves.fastOutSlowIn,
                            ),
                          ],
                        ),
                      ),
                    ))),
    );
  }
}
