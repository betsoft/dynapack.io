import 'dart:convert';
import 'dart:developer' as developer;

import 'package:dynapack/global.dart';
import 'package:dynapack/widgets/app_bar_floating.dart';
import 'package:dynapack/widgets/menu_actions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'customer_view.dart';
import 'drawer.dart';
import 'models/customers.dart';
import 'dart:io' show Platform;
import 'package:material_floating_search_bar/material_floating_search_bar.dart';


class CustomersView extends StatefulWidget {
  const CustomersView({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _CustomersViewState createState() => _CustomersViewState();
}

class _CustomersViewState extends State<CustomersView> {
  late Future<List<Customers>> futureCustomers;
  TextEditingController editingController = TextEditingController();
  late List<Customers> customers;
  late List<Customers> filter;
  bool doItJustOnce = false;

  @override
  void initState() {
    super.initState();

    futureCustomers = fetchCustomers();
  }

  Future<bool> showExitPopup() async {
    return await showDialog(
          //show confirm dialogue
          //the return value will be from "Yes" or "No" options
          context: context,
          builder: (context) => Platform.isIOS ? CupertinoAlertDialog(
            title: Text(AppLocalizations.of(context).appExitTitle),
            content: Text(AppLocalizations.of(context).appExitDescription),
            actions: [
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(false),
                //return false when click on "NO"
                child: Text(AppLocalizations.of(context).no),
              ),
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(true),
                //return true when click on "Yes"
                child: Text(AppLocalizations.of(context).yes),
              ),
            ],
          ) : AlertDialog(
            title: Text(AppLocalizations.of(context).appExitTitle),
            content: Text(AppLocalizations.of(context).appExitDescription),
            actions: [
              TextButton(onPressed: () => Navigator.of(context).pop(false), child: Text(AppLocalizations.of(context).no),),
              TextButton(onPressed: () => Navigator.of(context).pop(true),
              child: Text(AppLocalizations.of(context).yes),),

            ],
          ),
        ) ??
        false; //if showDialouge had returned null, then return false
  }

  Future<List<Customers>> fetchCustomers() async {
    final response = await http.get(
        Uri.parse(server + '/customers?limit=1'),
        headers: headers);
    if (response.statusCode == 200) {
      List responseJson = jsonDecode(response.body);

      var risposta = responseJson.map((e) => Customers.fromJson(e));
      return risposta.toList();
    } else {
      throw Exception('Failed to load album');
    }
  }



  @override
  Widget build(BuildContext context) {
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

    return WillPopScope(
      onWillPop: showExitPopup,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        drawer: AppDrawer('Customers'),
        drawerEnableOpenDragGesture: false,
        drawerEdgeDragWidth: MediaQuery.of(context).size.width,
        /*appBar: AppBar(
          actions: [
            MenuActions()
          ],
          title: Text(widget.title),
        ),*/
        body: FloatingSearchBar(
            hint: 'Cerca...',
            hintStyle: TextStyle(color: Colors.white),
            backgroundColor: Colors.blue,
            borderRadius: BorderRadius.circular(10),
            iconColor: Colors.white,
            scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
            transitionDuration: const Duration(milliseconds: 300),
            transitionCurve: Curves.easeInOut,
            physics: const BouncingScrollPhysics(),
            axisAlignment: isPortrait ? 0.0 : -1.0,
            openAxisAlignment: 0.0,
            width: isPortrait ? 600 : 1080,
            debounceDelay: const Duration(milliseconds: 500),
            onQueryChanged: (query) {
              // Call your model, bloc, controller here.
            },
            // Specify a custom transition to be used for
            // animating between opened and closed stated.
            transition: CircularFloatingSearchBarTransition(),
            actions: [
              MenuActions()
            ],
            builder: (context, transition) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Material(
                  color: Colors.white,
                  elevation: 4.0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: Colors.accents.map((color) {
                      return Container(height: 112, color: color);
                    }).toList(),
                  ),
                ),
              );
            },
            body: Scaffold(
              body: SafeArea(
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).viewPadding.top,
                      ),
                      Expanded(
                        child: FutureBuilder<List<Customers>>(
                          future: futureCustomers,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              customers = snapshot.data!;
                              customers.sort((a, b) => a.Name1.compareTo(b.Name1));
                              return ListView.builder(
                                shrinkWrap: true,
                                itemCount: customers.length,
                                itemBuilder: (context, index) {
                                  String ragSoc = '';
                                  String address = customers.elementAt(index).Address +
                                      ', ' +
                                      customers.elementAt(index).city!.name +
                                      ', ' +
                                      customers.elementAt(index).Cap;
                                  if(customers.elementAt(index).city!.province != null){
                                    address += ', ' + customers.elementAt(index).city!.province!.code;
                                    if(customers.elementAt(index).city!.province!.region != null){
                                      address += ', ' +
                                          customers
                                              .elementAt(index)
                                              .city!
                                              .province!
                                              .region!
                                              .name;
                                      if(customers.elementAt(index).city!.province!.region!.country != null){
                                        address +=  ', ' +
                                            customers
                                                .elementAt(index)
                                                .city!
                                                .province!
                                                .region!
                                                .country!
                                                .name;
                                      }
                                    }
                                  }


                                  if (customers.elementAt(index).Name1 != 'null') {
                                    if (customers.elementAt(index).Name2 != 'null') {
                                      if (customers.elementAt(index).Name3 != 'null') {
                                        ragSoc = customers.elementAt(index).Name1 +
                                            ', ' +
                                            customers.elementAt(index).Name2 +
                                            ', ' +
                                            customers.elementAt(index).Name3;
                                      } else {
                                        ragSoc = customers.elementAt(index).Name1 +
                                            ', ' +
                                            customers.elementAt(index).Name2;
                                      }
                                    } else {
                                      if (customers.elementAt(index).Name3 != 'null') {
                                        ragSoc = customers.elementAt(index).Name1 +
                                            ', ' +
                                            customers.elementAt(index).Name3;
                                      } else {
                                        ragSoc = customers.elementAt(index).Name1;
                                      }
                                    }
                                  } else if (customers.elementAt(index).Name2 !=
                                      'null') {
                                    if (customers.elementAt(index).Name3 != 'null') {
                                      ragSoc = customers.elementAt(index).Name2 +
                                          ', ' +
                                          customers.elementAt(index).Name3;
                                    } else {
                                      ragSoc = customers.elementAt(index).Name2;
                                    }
                                  } else if (customers.elementAt(index).Name3 !=
                                      'null') {
                                    ragSoc = customers.elementAt(index).Name3;
                                  }
                                  return Slidable(
                                      child: Container(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Customer(
                                                      customer:
                                                      customers.elementAt(index))),
                                            );
                                          },
                                          child: ListTile(
                                            title: Text(
                                              ragSoc,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            subtitle: Text(address,
                                                style: TextStyle(fontSize: 11)),
                                          ),
                                        ),
                                      ),
                                      actions: <Widget>[
                                        new IconSlideAction(
                                            icon: Icons.delete, color: Colors.red),
                                        new IconSlideAction(
                                          icon: Icons.archive,
                                          color: Colors.blueGrey,
                                        ),
                                      ],
                                      secondaryActions: <Widget>[
                                        new IconSlideAction(
                                            icon: Icons.delete, color: Colors.red),
                                        new IconSlideAction(
                                          icon: Icons.archive,
                                          color: Colors.blueGrey,
                                        ),
                                      ],
                                      actionPane: SlidableDrawerActionPane());
                                },
                              );
                            } else if (snapshot.hasError) {
                              return Text("${snapshot.error}");
                            }
                            return Center(
                                child: Container(
                                    width: 100,
                                    height: 100,
                                    child: CircularProgressIndicator()));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
      ),
    );
  }

}
