import 'package:dynapack/widgets/anagraphic_data.dart';
import 'package:dynapack/widgets/anagraphic_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'models/products.dart';

class Product extends StatefulWidget {
  final Products product;


  const Product({Key? key, required this.product}) : super(key: key);

  @override
  _CustomerState createState() => _CustomerState();
}

class _CustomerState extends State<Product> {
  late String description;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    description = widget.product.description1;
    if(widget.product.description2 != 'null' && widget.product.description2 != ''){
      description += '\n' + widget.product.description2;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text(widget.product.description1),
      ),
      body: Center(
        child: Column(
          children: [

            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    children: [
                      ExpansionTile(
                        title: Text(AppLocalizations.of(context).registry),
                        initiallyExpanded: true,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    widget.product.code != 'null'
                                        ? AnagraphicTitle(
                                        title: AppLocalizations.of(context).code)
                                        : Container(),
                                    widget.product.code != 'null'
                                        ? AnagraphicData(
                                        data: widget.product.code)
                                        : Container(),
                                    AnagraphicTitle(
                                        title: AppLocalizations.of(context).description),
                                    AnagraphicData(
                                        data: description),
                                    AnagraphicTitle(title: AppLocalizations.of(context).dimension),
                                    AnagraphicData(
                                        data: widget.product.dimension1.toString() + 'x' + widget.product.dimension2.toString() + 'x' + widget.product.dimension3.toString() ),
                                    widget.product.quality != null ? widget.product.quality!.code != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).qualityCode) : Container() : Container(),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    widget.product.externalCode != 'null'
                                        ? AnagraphicTitle(
                                        title: AppLocalizations.of(context).externalCode)
                                        : Container(),
                                    widget.product.externalCode != 'null'
                                        ? AnagraphicData(
                                        data: widget.product.externalCode)
                                        : Container(),
                                    AnagraphicTitle(title: AppLocalizations.of(context).mainMeasureUnit),
                                    AnagraphicData(
                                        data: widget.product.mainMeasureUnit + ', ' + widget.product.alternativeMeasureUnit),
                                    AnagraphicTitle(title: AppLocalizations.of(context).weight),
                                    AnagraphicData(
                                        data: widget.product.grammage.toString()),
                                    AnagraphicTitle(title: AppLocalizations.of(context).state),
                                    AnagraphicData(
                                        data: widget.product.status),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      widget.product.category != null ? ExpansionTile(
                        title: Text(AppLocalizations.of(context).category),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).categoryCode),
                                    AnagraphicData(
                                        data: widget.product.category!.code),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).categoryExternalCode),
                                    AnagraphicData(
                                        data: widget.product.category!.externalCode),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).categoryDescription),
                                    AnagraphicData(
                                        data: widget.product.category!.description),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ) : Container(),
                      widget.product.conai != null && widget.product.group != null ? ExpansionTile(
                        title: Text('Conai & Gruppo prodotti'),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              widget.product.conai != null ? Container(
                                width:
                                MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).conaiCode),
                                    AnagraphicData(
                                        data: widget.product.conai!.code),
                                    AnagraphicTitle(title: AppLocalizations.of(context).conaiDescription),
                                    AnagraphicData(
                                        data: widget.product.conai!.description),
                                    AnagraphicTitle(title: AppLocalizations.of(context).conaiWeight),
                                    AnagraphicData(
                                        data: widget.product.conaiWeight),
                                  ],
                                ),
                              ) : Container(),
                              widget.product.conai != null ? Container(
                                width:
                                MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).groupCode),
                                    AnagraphicData(
                                        data: widget.product.group!.code),
                                    AnagraphicTitle(title: AppLocalizations.of(context).groupExternalCode),
                                    AnagraphicData(
                                        data: widget.product.group!.externalCode),
                                    AnagraphicTitle(title: AppLocalizations.of(context).groupDescription),
                                    AnagraphicData(
                                        data: widget.product.group!.description),
                                  ],
                                ),
                              ) : Container(),
                            ],
                          )
                        ],
                      ) : Container(),
                      widget.product.vat != null ? ExpansionTile(
                        title: Text('Iva'),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: MediaQuery.of(context).size.width *
                                      0.5,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      widget.product.vat.code != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaCode) : Container(),
                                      widget.product.vat.code != 'null' ? AnagraphicData(data: widget.product.vat.code)  : Container(),
                                      widget.product.vat.externalCode != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaExternalCode)  : Container(),
                                      widget.product.vat.externalCode != 'null' ? AnagraphicData(data: widget.product.vat.externalCode)  : Container(),
                                      ],
                                  )),
                              Container(
                                  width: MediaQuery.of(context).size.width *
                                      0.5,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      widget.product.vat.description != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaDescription)  : Container(),
                                      widget.product.vat.description != 'null' ? AnagraphicData(data: widget.product.vat.description)  : Container(),
                                      widget.product.vat.percentage != 0 ? AnagraphicTitle(title: AppLocalizations.of(context).ivaPercentage)  : Container(),
                                      widget.product.vat.percentage != 0 ? AnagraphicData(data: widget.product.vat.percentage.toString()) : Container(),
                                    ],
                                  ))
                            ],
                          )
                        ],
                      ) : Container(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
