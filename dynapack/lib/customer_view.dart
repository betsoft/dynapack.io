import 'package:dynapack/widgets/anagraphic_data.dart';
import 'package:dynapack/widgets/anagraphic_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'models/customers.dart';
import 'main.dart';

class Customer extends StatefulWidget {
  final Customers customer;

  const Customer({Key? key, required this.customer}) : super(key: key);

  @override
  _CustomerState createState() => _CustomerState();
}

class _CustomerState extends State<Customer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text(widget.customer.Name1),
      ),
      body: Center(
        child: Column(
          children: [

            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    children: [
                      ExpansionTile(
                        title: Text(AppLocalizations.of(context).registry),
                        initiallyExpanded: true,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(
                                        title: AppLocalizations.of(context).businessName + ' 1'),
                                    AnagraphicData(
                                        data: widget.customer.Name1),
                                    widget.customer.Name2 != 'null'
                                        ? AnagraphicTitle(
                                            title: AppLocalizations.of(context).businessName + ' 2')
                                        : Container(),
                                    widget.customer.Name2 != 'null'
                                        ? AnagraphicData(
                                            data: widget.customer.Name2)
                                        : Container(),
                                    widget.customer.Name3 != 'null'
                                        ? AnagraphicTitle(
                                            title: AppLocalizations.of(context).businessName + ' 3')
                                        : Container(),
                                    widget.customer.Name3 != 'null'
                                        ? AnagraphicData(
                                            data: widget.customer.Name3)
                                        : Container(),
                                    AnagraphicTitle(title: AppLocalizations.of(context).vatCode),
                                    AnagraphicData(
                                        data: widget.customer.VatNumber),
                                    AnagraphicTitle(title: AppLocalizations.of(context).fiscalCode),
                                    AnagraphicData(
                                        data: widget.customer.FiscalCode),
                                    AnagraphicTitle(title: AppLocalizations.of(context).code),
                                    AnagraphicData(
                                        data: widget.customer.Code),
                                    AnagraphicTitle(title: AppLocalizations.of(context).externalCode),
                                    AnagraphicData(
                                        data: widget.customer.ExternalCode),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).address),
                                    AnagraphicData(
                                        data: widget.customer.Address),
                                    AnagraphicTitle(title: AppLocalizations.of(context).city),
                                    AnagraphicData(
                                        data: widget.customer.city!.name),
                                    AnagraphicTitle(title: AppLocalizations.of(context).cap),
                                    AnagraphicData(data: widget.customer.Cap),
                                    widget.customer.city!.province == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).province),
                                    widget.customer.city!.province == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.city!.province!.name),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).region),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() :AnagraphicData(
                                        data: widget.customer.city!.province!
                                            .region!.name),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : widget.customer.city!.province!.region!.country == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).nation),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : widget.customer.city!.province!.region!.country == null ? Container() : AnagraphicData(
                                        data: widget.customer.city!.province!
                                            .region!.country!.name),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).telephone),
                                    AnagraphicData(
                                        data: widget.customer.PhoneNumber),
                                    AnagraphicTitle(title: AppLocalizations.of(context).fax),
                                    AnagraphicData(
                                        data: widget.customer.FaxNumber),
                                    AnagraphicTitle(title: AppLocalizations.of(context).email),
                                    AnagraphicData(
                                        data: widget.customer.Email),
                                    AnagraphicTitle(title: AppLocalizations.of(context).discount + ' 1'),
                                    AnagraphicData(
                                        data: widget.customer.Discount1),
                                    AnagraphicTitle(title: AppLocalizations.of(context).discount + ' 2'),
                                    AnagraphicData(
                                        data: widget.customer.Discount2),
                                    AnagraphicTitle(title: AppLocalizations.of(context).customerType),
                                    AnagraphicData(
                                        data: widget.customer.CustomerType),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      ExpansionTile(
                        title: Text(AppLocalizations.of(context).paymentMethod),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(title: AppLocalizations.of(context).description),
                                    AnagraphicData(
                                        data: widget
                                            .customer.payment!.description),
                                    AnagraphicTitle(title: AppLocalizations.of(context).code),
                                    AnagraphicData(
                                        data: widget.customer.payment!.code),
                                    AnagraphicTitle(title: AppLocalizations.of(context).externalCode),
                                    AnagraphicData(
                                        data: widget
                                            .customer.payment!.externalCode),
                                    AnagraphicTitle(title: AppLocalizations.of(context).discount + ' 1'),
                                    AnagraphicData(
                                        data: widget
                                            .customer.payment!.discount1
                                            .toString()),
                                    AnagraphicTitle(title: AppLocalizations.of(context).discount + ' 2'),
                                    AnagraphicData(
                                        data: widget
                                            .customer.payment!.discount2
                                            .toString()),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    widget.customer.currency == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).currencyCode),
                                    widget.customer.currency == null ? Container() : AnagraphicData(
                                        data: widget.customer.currency!.code),
                                    widget.customer.currency == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).currencyDescription),
                                    widget.customer.currency == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.currency!.description),
                                    widget.customer.currency == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).deadlineCode),
                                    widget.customer.deadline == null ? Container() : AnagraphicData(
                                        data: widget.customer.deadline!.code),
                                    widget.customer.deadline == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).deadlineExternalCode),
                                    widget.customer.deadline == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.deadline!.externalCode),
                                    widget.customer.deadline == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).deadlineDescription),
                                    widget.customer.deadline == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.deadline!.description),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      ExpansionTile(
                        title: Text(AppLocalizations.of(context).delivery),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    widget.customer.transport == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).transportCode),
                                    widget.customer.transport == null ? Container() : AnagraphicData(
                                        data: widget.customer.transport!.code),
                                    widget.customer.transport == null ? Container() : AnagraphicTitle(
                                        title:
                                        AppLocalizations.of(context).transportDescription),
                                    widget.customer.transport == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.transport!.description),
                                    widget.customer.transport == null ? Container() : AnagraphicTitle(title: AppLocalizations.of(context).copiesNumber),
                                    AnagraphicData(
                                        data: widget
                                            .customer.transport!.copiesNumber
                                            .toString()),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    widget.customer.carriage == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).carriageCode),
                                    widget.customer.carriage == null ? Container() : AnagraphicData(
                                        data: widget.customer.carriage!.code),
                                    widget.customer.carriage == null ? Container() : AnagraphicTitle(
                                        title:
                                        AppLocalizations.of(context).carriageDescription),
                                    widget.customer.carriage == null ? Container() : AnagraphicData(
                                        data: widget
                                            .customer.carriage!.description),
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                    MediaQuery.of(context).size.width * 0.33,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    AnagraphicTitle(
                                        title: AppLocalizations.of(context).delayCity),
                                    AnagraphicData(
                                        data: widget
                                            .customer.city!.deliveryDelay
                                            .toString()),
                                    widget.customer.city!.province == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).delayProvince),
                                    widget.customer.city!.province == null ? Container() : AnagraphicData(
                                        data: widget.customer.city!.province!
                                            .deliveryDelay
                                            .toString()),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).delayRegion),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : AnagraphicData(
                                        data: widget.customer.city!.province!
                                            .region!.deliveryDelay
                                            .toString()),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : widget.customer.city!.province!.region!.country == null ? Container() : AnagraphicTitle(
                                        title: AppLocalizations.of(context).delayNation),
                                    widget.customer.city!.province == null ? Container() : widget.customer.city!.province!.region == null ? Container() : widget.customer.city!.province!.region!.country == null ? Container() : AnagraphicData(
                                        data: widget.customer.city!.province!
                                          .region!.country!.deliveryDelay
                                            .toString()),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      ExpansionTile(
                        title: Text('Misc'),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: MediaQuery.of(context).size.width *
                                      0.33,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.code != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaCode) : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.code != 'null' ? AnagraphicData(data: widget.customer.vat!.code)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.externalCode != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaExternalCode)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.externalCode != 'null' ? AnagraphicData(data: widget.customer.vat!.externalCode)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.description != 'null' ? AnagraphicTitle(title: AppLocalizations.of(context).ivaDescription)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.description != 'null' ? AnagraphicData(data: widget.customer.vat!.description)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.percentage != 0 ? AnagraphicTitle(title: AppLocalizations.of(context).ivaPercentage)  : Container(),
                                      widget.customer.vat == null ? Container() : widget.customer.vat!.percentage != 0 ? AnagraphicData(data: widget.customer.vat!.percentage.toString()) : Container(),

                                    ],
                                  ))
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
