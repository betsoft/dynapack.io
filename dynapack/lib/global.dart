import 'package:dynapack/models/product_transaction_rows.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login.dart';

Map<String, String> headers = {};

late final storage;

List<ProductTransactionRows> deliverRows = [];
List<int> quantities = [];
List<int> udcs = [];

String server = "http://dev.api.dynapack.io";

void logout(BuildContext context) async {
  await storage.delete(key: "username");
  await storage.delete(key: "password");
  await prefs.remove("isLogged");
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => Login()));
}