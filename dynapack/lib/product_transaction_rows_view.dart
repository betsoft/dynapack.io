import 'dart:convert';
import 'dart:io' show Platform;

import 'package:dynapack/widgets/deliver_view.dart';
import 'package:dynapack/widgets/menu_actions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:http/http.dart' as http;
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'dart:developer' as developer;
import 'drawer.dart';
import 'global.dart';
import 'models/product_transaction_rows.dart';

class ProductTransactionRowsView extends StatefulWidget {
  const ProductTransactionRowsView({Key? key}) : super(key: key);

  @override
  _ProductTransactionRowsViewState createState() =>
      _ProductTransactionRowsViewState();
}

class _ProductTransactionRowsViewState
    extends State<ProductTransactionRowsView> {
  late Future<List<ProductTransactionRows>> futureRows;
  TextEditingController editingController = TextEditingController();
  List<ProductTransactionRows> rows = [];
  late List<ProductTransactionRows> filter;
  final ScrollController _scrollController = ScrollController();
  int skip = 0;
  bool doItJustOnce = false;

  @override
  void initState() {
    super.initState();

    futureRows = fetchProducts();
  }

  Future<List<ProductTransactionRows>> fetchProducts() async {
    final response = await http
        .get(Uri.parse(server + '/productTransactionRows'), headers: headers);
    if (response.statusCode == 200) {
      List responseJson = await jsonDecode(response.body);

      developer.log(responseJson.toString());
      var risposta =
          responseJson.map((e) => ProductTransactionRows.fromJson(e));
      return risposta.toList();
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return Scaffold(
      drawerEnableOpenDragGesture: false,
      drawerEdgeDragWidth: MediaQuery.of(context).size.width,
      drawer: AppDrawer('ProductTransactionRows'),
      floatingActionButton: deliverRows.length == 0 ? null : Container(
        child: FittedBox(
            child: Stack(
          alignment: Alignment(1.4, -1.5),
          children: [
            FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DeliverView()));
              },
              child: Icon(
                Icons.directions_bus_filled_outlined,
                color: Colors.white,
              ),
              backgroundColor: Colors.blue,
            ),
            Container(
              child: Center(
                child: Text(deliverRows.length.toString(),
                    style: TextStyle(color: Colors.white)),
              ),
              padding: EdgeInsets.all(8),
              constraints: BoxConstraints(minHeight: 32, minWidth: 32),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 1,
                      blurRadius: 5,
                      color: Colors.black.withAlpha(50))
                ],
                borderRadius: BorderRadius.circular(16),
                color: Colors.red,
              ),
            ),
          ],
        )),
      ),
      body: FloatingSearchBar(
          hint: 'Cerca...',
          hintStyle: TextStyle(color: Colors.white),
          backgroundColor: Colors.blue,
          borderRadius: BorderRadius.circular(10),
          iconColor: Colors.white,
          scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
          transitionDuration: const Duration(milliseconds: 300),
          transitionCurve: Curves.easeInOut,
          physics: const BouncingScrollPhysics(),
          axisAlignment: isPortrait ? 0.0 : -1.0,
          openAxisAlignment: 0.0,
          width: isPortrait ? 600 : 1080,
          debounceDelay: const Duration(milliseconds: 500),
          onQueryChanged: (query) {
            // Call your model, bloc, controller here.
          },
          // Specify a custom transition to be used for
          // animating between opened and closed stated.
          transition: CircularFloatingSearchBarTransition(),
          actions: [MenuActions()],
          builder: (context, transition) {
            return ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Material(
                color: Colors.white,
                elevation: 4.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: Colors.accents.map((color) {
                    return Container(height: 112, color: color);
                  }).toList(),
                ),
              ),
            );
          },
          body: Scaffold(
              body: SafeArea(
            child: Center(
                child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).viewPadding.top,
                ),
                Expanded(
                  child: FutureBuilder<List<ProductTransactionRows>>(
                      future: futureRows,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          rows = snapshot.data!;

                          return ListView.builder(
                            shrinkWrap: true,
                            itemCount: rows.length,
                            itemBuilder: (context, index) {
                              String firstRow;
                              String secondRow;
                              if (rows.elementAt(index).delivery != null) {
                                if (rows
                                        .elementAt(index)
                                        .delivery!
                                        .salesOrderRow !=
                                    null)
                                  firstRow = rows
                                          .elementAt(index)
                                          .delivery!
                                          .salesOrderRow!
                                          .order!
                                          .customer
                                          .Code +
                                      ', ' +
                                      rows
                                          .elementAt(index)
                                          .delivery!
                                          .salesOrderRow!
                                          .order!
                                          .customer
                                          .Name1;
                                else
                                  firstRow = rows
                                          .elementAt(index)
                                          .delivery!
                                          .productionOrderRow!
                                          .order!
                                          .customer
                                          .Code +
                                      ', ' +
                                      rows
                                          .elementAt(index)
                                          .delivery!
                                          .productionOrderRow!
                                          .order!
                                          .customer
                                          .Name1;
                              } else if (rows
                                      .elementAt(index)
                                      .productionOrderRow !=
                                  null) {
                                firstRow = rows
                                        .elementAt(index)
                                        .productionOrderRow!
                                        .order!
                                        .customer
                                        .Code +
                                    ', ' +
                                    rows
                                        .elementAt(index)
                                        .productionOrderRow!
                                        .order!
                                        .customer
                                        .Name1;
                              } else {
                                firstRow = rows.elementAt(index).product!.code +
                                    ', ' +
                                    rows.elementAt(index).product!.description1;
                              }
                              secondRow = rows.elementAt(index).product!.code +
                                  ', ' +
                                  rows.elementAt(index).product!.description1;
                              return Slidable(
                                  child: Container(
                                    child: GestureDetector(
                                      onTap: () {
                                        /*
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => ProductTransactionRowView(
                                                      product:
                                                      products.elementAt(index))),
                                            );*/
                                      },
                                      child: ListTile(
                                        title: Text(
                                          firstRow,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        subtitle: Text(secondRow,
                                            style: TextStyle(fontSize: 11)),
                                      ),
                                    ),
                                  ),
                                  actions: const <Widget>[
                                    IconSlideAction(
                                        icon: Icons.delete, color: Colors.red),
                                    IconSlideAction(
                                      icon: Icons.edit,
                                      color: Colors.blueGrey,
                                    ),
                                  ],
                                  secondaryActions: <Widget>[
                                    IconSlideAction(
                                      icon: Icons.add_circle,
                                      color: Colors.green,
                                      onTap: () {
                                        setState(() {
                                          if (Platform.isIOS ||
                                              Platform.isAndroid) {
                                            TextEditingController quantita = new TextEditingController();
                                            TextEditingController udc = new TextEditingController();
                                            showCupertinoDialog(
                                                context: context,
                                                builder: (context) {
                                                  return CupertinoAlertDialog(
                                                    title: const Text(
                                                        "Seleziona la quantità di prodotto e il numero di bancali "),
                                                    content: SingleChildScrollView(
                                                      scrollDirection: Axis.vertical,
                                                      child: Material(
                                                        color: Colors.transparent,
                                                        child: Column(

                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .stretch,
                                                          children: <Widget>[
                                                            Padding(padding: EdgeInsets.only(top: 10),),
                                                            TextField(
                                                              controller: udc,
                                                              keyboardType: TextInputType.number,
                                                              decoration: InputDecoration(
                                                                  border:
                                                                      OutlineInputBorder(),
                                                                  labelText:
                                                                      "Numero di bancali"),
                                                            ),
                                                            Padding(padding: EdgeInsets.all(10),),
                                                            TextField(
                                                              controller: quantita,
                                                              keyboardType: TextInputType.number,
                                                              decoration: InputDecoration(
                                                                  border:
                                                                      OutlineInputBorder(),
                                                                  labelText:
                                                                      "Quantità"),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    actions: <Widget>[

                                                      CupertinoDialogAction(child: Text('No'), onPressed: (){
                                                        Navigator.pop(context);
                                                      },),
                                                      CupertinoDialogAction(child: Text('Yes'), onPressed: (){
                                                        deliverRows
                                                            .add(rows.elementAt(index));
                                                        quantities.add(int.parse(quantita.text));
                                                        udcs.add(int.parse(udc.text));
                                                        setState(() {
                                                          Navigator.pop(context);
                                                        });
                                                      },),


                                                    ],
                                                  );
                                                });
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                  actionPane: SlidableDrawerActionPane());
                            },
                          );
                        } else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        } else {
                          return Center(
                              child: Container(
                                  height: 100,
                                  width: 100,
                                  child: CircularProgressIndicator()));
                        }
                      }),
                ),
              ],
            )),
          ))),
    );
  }
}
